package com.example.cse438.studio4

import android.app.ActionBar
import android.app.Dialog
import android.content.Context
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source

class App {
    companion object {
        var firebaseAuth: FirebaseAuth? = null

        fun openReviewDialog(context: Context, itemId: String) {
            val dialog = Dialog(context)

            dialog.setContentView(R.layout.dialog_add_review)

            val window = dialog.window
            window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

            dialog.findViewById<Button>(R.id.close).setOnClickListener {
                dialog.dismiss()
            }

            dialog.findViewById<Button>(R.id.submit).setOnClickListener {
                // TODO: Finish implementing using given pseudocode
                var UIbody = R.id.body
                var UIanonymous = R.id.anonymous
                var user = firebaseAuth?.getCurrentUser()

                var userID = null
                if (user != null && UIbody.toString().length > 0){
                    var fbInstance = FirebaseFirestore.getInstance()
                    //var thisItem = fbInstance.document(itemId).collection("items")
                    var documentRef = fbInstance.collection("items").document(itemId)

                    val source = Source.CACHE

                    documentRef.get(source).addOnCompleteListener{ task ->
                        if(task.isSuccessful){
                            val document = task.result!!

                            var docData = document.data

                            var revyooz = arrayListOf<Review>()

                            revyooz = (ArrayList<Review>) docData.get("reviews")
                        }

                    }
                }
            }

            dialog.show()
        }
    }
}